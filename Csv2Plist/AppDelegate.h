//
//  AppDelegate.h
//  Csv2Plist
//
//  Created by コマツバラ ヒロシ on 13/02/08.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (unsafe_unretained) IBOutlet NSTextView *textView;

@end
