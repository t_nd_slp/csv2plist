//
//  main.m
//  Csv2Plist
//
//  Created by コマツバラ ヒロシ on 13/02/08.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
