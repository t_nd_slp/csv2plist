//
//  AppDelegate.m
//  Csv2Plist
//
//  Created by コマツバラ ヒロシ on 13/02/08.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import "AppDelegate.h"

#define PlistHeader @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n<plist version=\"1.0\">\n<array>\n"
#define PlistFooter @"</array>\n</plist>"
#define ArraySeparator @"^"

@interface AppDelegate ()
@property (nonatomic,strong) NSArray *keys;
@property (nonatomic,strong) NSString *separator;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    self.separator = @",";
}

- (IBAction)radioGropuChanged:(NSButtonCell *)sender
{
    if(sender.integerValue==0) self.separator = @",";
    else self.separator = @"\t";
}

- (IBAction)convert:(NSButton *)sender
{
    //make array from textView
    NSMutableArray *lines = [NSMutableArray array];
    [self.textView.string enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
        [lines addObject:line];
    }];
    
    NSMutableArray *cells = [NSMutableArray array];
    for (NSString *line in lines) {
        [cells addObject: [line componentsSeparatedByString:self.separator]];
    }
    self.keys = cells[0];       //first line is key
    [cells removeObjectAtIndex:0];

    NSString *xml = @"";
    //assemble plist code
    for (NSArray *line in cells)
    {
        xml = [NSString stringWithFormat:@"%@%@",xml,[self line2DictionaryXML:line]];
    }
    
    xml = [NSString stringWithFormat:@"%@%@%@",PlistHeader,xml,PlistFooter];
    [self.textView setString:xml];
}

- (NSString*)line2DictionaryXML:(NSArray*)line
{
    NSString *lineString = @"\t<dict>\n";
    for (int i=0; i<[line count]; i++)
    {
        lineString = [NSString stringWithFormat:@"%@\t\t<key>%@</key>\n",lineString, self.keys[i]];
        lineString = [NSString stringWithFormat:@"%@%@",lineString, [self factor2XML:line[i]]];
    }
    lineString = [NSString stringWithFormat:@"%@\t</dict>\n",lineString];
    
    return lineString;
}

- (NSString*)factor2XML:(NSString*)factor
{
    //check Array
    NSRange range = [factor rangeOfString:ArraySeparator];
    if (range.location != NSNotFound) {
        return [self Array2ArrayXML:[factor componentsSeparatedByString:ArraySeparator]];
    }
    
    if([self isNumeric:factor])
        return [NSString stringWithFormat:@"\t\t<real>%@</real>\n",factor];
    else
        return [NSString stringWithFormat:@"\t\t<string>%@</string>\n",factor];    
}

- (NSString*)Array2ArrayXML:(NSArray*)line
{
    NSString *lineString = @"\t\t<array>\n";
    for (int i=0; i<[line count]; i++)
    {
        lineString = [NSString stringWithFormat:@"%@\t%@",lineString, [self factor2XML:line[i]]];
    }
    lineString = [NSString stringWithFormat:@"%@\t\t</array>\n",lineString];
    
    return lineString;
}

- (BOOL)isNumeric:(NSString*) hexText
{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    NSNumber* number = [numberFormatter numberFromString:hexText];
    return (number != nil) ? YES : NO;
}

@end
